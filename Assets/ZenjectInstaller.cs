using Assets.Scripts.Engineer;
using Assets.Scripts.Locations;
using Assets.Scripts.MonBehavior;
using Assets.Scripts.MonBehavior.Obstacles;
using Assets.Scripts.Train;
using Zenject;

namespace Assets
{
    public class ZenjectInstaller : MonoInstaller<ZenjectInstaller>
    {
        public TrainConfig TrainConfig;
        public SteamEngineLevels InitialSteamEngineLevels;
        public SteamEngineRates InitialSteamEngineRates;
        public Location StartLocation = Location.Cab;
        public LocationController[] Locations;
        public ObstacleManager ObstacleManager;
        public GameController GameController;
        public PlayerController PlayerController;

        public override void InstallBindings()
        {
            Container.BindInstance(InitialSteamEngineLevels).AsSingle();
            Container.BindInstance(InitialSteamEngineRates).AsSingle();
            Container.BindInstance(TrainConfig).AsSingle();
            Container.BindInstance(Action.Actions).AsSingle();
            Container.Bind<SteamEngine>().AsSingle();
            Container.Bind<Train>().AsSingle();
            Container.Bind<Engineer>().AsSingle().WithArguments(StartLocation);

            Container.Bind<GameStateManager>().AsSingle();
            Container.BindInstance(Locations).AsSingle();
            Container.BindInstance(ObstacleManager).AsSingle();
            Container.BindInstance(GameController);
            Container.BindInstance(PlayerController);

        }
    }
}