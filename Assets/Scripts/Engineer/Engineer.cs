﻿using System;
using Assets.Scripts.Locations;

namespace Assets.Scripts.Engineer
{
    public class Engineer
    {
        private readonly Train.Train _train;

        public Location CurrentLocation { get; set; }
        public Location TargetLocation { get; private set; }

        public MovementState MovementState { get; private set; }
        public event EventHandler OnMovementStateChange;

        public Engineer(Train.Train train, Location startLocation)
        {
            MovementState = MovementState.Walking;
            _train = train;
            CurrentLocation = TargetLocation = startLocation;
        }

        public void StartMoveNext()
        {
            if (CanMoveNext)
            {
                TargetLocation = CurrentLocation + 1;
            }
        }

        public bool CanMoveNext { get { return !CurrentLocation.IsLast(); } }

        public void StartMovePrevious()
        {
            if (CanMovePrevious)
            {
                TargetLocation = CurrentLocation - 1;
            }
        }

        public bool CanMovePrevious { get { return !CurrentLocation.IsFirst(); } }

        public void AddCoal()
        {
            _train.Engine.BurnCoal();
        }

        public void SetMovementState(MovementState movementState)
        {
            MovementState = movementState;
            if (OnMovementStateChange != null)
            {
                OnMovementStateChange.Invoke(this, new EventArgs());
            }
        }
    }
}
