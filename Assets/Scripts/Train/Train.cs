﻿using System;
using Assets.Scripts.Obstacles;

namespace Assets.Scripts.Train
{
    public class Train
    {
        private readonly TrainConfig _trainConfig;

        public float PlowTimer { get; set; }

        public event EventHandler<TrainHitEventArgs> OnTrainHit;

        public Train(TrainConfig trainConfig, SteamEngine engine)
        {
            _trainConfig = trainConfig;
            Engine = engine;
        }

        public SteamEngine Engine { get; private set; }

        public float Speed { get { return Engine.EngineLevels.SteamLevel; } }

        public float DistanceTravelled { get; set; }
        public bool HasPlow { get { return PlowTimer > 0; } }

        public void AttachPlow()
        {
            PlowTimer += _trainConfig.PlowRefreshTime;
        }

        public void DecayPlow(float deltaTime)
        {
            PlowTimer -= deltaTime;
            if (PlowTimer < 0)
            {
                PlowTimer = 0;
            }
        }

        public void Hit(ObstacleType obstacleType, int coalToLose)
        {
            var actualCoalLost = Engine.EngineLevels.TryRemoveCoal(coalToLose);
            if (OnTrainHit != null)
            {
                OnTrainHit.Invoke(this, new TrainHitEventArgs(obstacleType, actualCoalLost));
            }
        }
    }
}
