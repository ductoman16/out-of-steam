using System;

namespace Assets.Scripts.Train
{
    [Serializable]
    public class SteamEngineLevels
    {
        public SteamEngineLevels(int coalLevel, float waterLevel, float fireLevel, float steamLevel, float maxSteam)
        {
            CoalLevel = coalLevel;
            WaterLevel = waterLevel;
            FireLevel = fireLevel;
            SteamLevel = steamLevel;
            MaxSteam = maxSteam;
        }

        public int CoalLevel;
        public float FireLevel;
        public float WaterLevel;
        public float SteamLevel;
        public float MaxSteam;

        /// <summary>
        ///     Attempts to remove coal, doesn't remove more coal than we have. Returns
        ///     the actual amount of coal removed.
        /// </summary>
        public int TryRemoveCoal(int coalToRemove)
        {
            var actualCoalLost = Math.Min(coalToRemove, CoalLevel);
            CoalLevel -= actualCoalLost;
            return actualCoalLost;
        }
    }
}