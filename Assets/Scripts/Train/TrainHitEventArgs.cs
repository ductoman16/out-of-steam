﻿using System;
using Assets.Scripts.Obstacles;

namespace Assets.Scripts.Train
{
    public class TrainHitEventArgs : EventArgs
    {
        public TrainHitEventArgs(ObstacleType obstacleTypeHit, int actualCoalLost)
        {
            ObstacleTypeHit = obstacleTypeHit;
            ActualCoalLost = actualCoalLost;
        }

        public ObstacleType ObstacleTypeHit { get; private set; }
        public int ActualCoalLost { get; private set; }
    }
}