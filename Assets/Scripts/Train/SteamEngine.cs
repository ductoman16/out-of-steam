﻿using System;

namespace Assets.Scripts.Train
{
    //Coal makes fire
    //Fire burns water, makes steam
    //Steam make speed
    public class SteamEngine
    {
        private readonly SteamEngineRates _steamEngineRates;

        public SteamEngineLevels EngineLevels { get; private set; }

        public SteamEngine(SteamEngineLevels engineLevels, SteamEngineRates steamEngineRates)
        {
            EngineLevels = engineLevels;
            _steamEngineRates = steamEngineRates;
        }

        public bool HasFuel { get { return EngineLevels.CoalLevel > 0 && EngineLevels.WaterLevel > 0; } }

        /// <summary>
        ///     Adds coal to the fire.
        /// </summary>
        public void BurnCoal()
        {
            var coalBurned = EngineLevels.TryRemoveCoal(_steamEngineRates.CoalPerShovel);
            //var coalBurned = Math.Min(_steamEngineRates.CoalPerShovel, EngineLevels.CoalLevel);
            //EngineLevels.CoalLevel -= coalBurned;
            EngineLevels.FireLevel += coalBurned * _steamEngineRates.CoalBurnRate;
        }



        /// <summary>
        ///     Burns water into steam based on fire level.
        /// </summary>
        public void BurnWater(float timeSinceLastBurn)
        {
            var waterToBurn = timeSinceLastBurn * EngineLevels.FireLevel;
            var waterBurned = Math.Min(waterToBurn, EngineLevels.WaterLevel);//Is there even enough left

            EngineLevels.WaterLevel -= waterBurned;
            EngineLevels.SteamLevel += waterBurned * _steamEngineRates.WaterToSteamRate;
            if (EngineLevels.SteamLevel > EngineLevels.MaxSteam)
            {
                EngineLevels.SteamLevel = EngineLevels.MaxSteam;
            }
        }

        /// <summary>
        ///     Reduces the fire level over time.
        /// </summary>
        public void FireBurnDown(float timeSinceLastBurnDown)
        {
            EngineLevels.FireLevel -= timeSinceLastBurnDown * _steamEngineRates.FireBurnDownRate;
            if (EngineLevels.FireLevel < 0)
            {
                EngineLevels.FireLevel = 0;
            }
        }

        /// <summary>
        ///     Releases steam pressure over time.
        /// </summary>
        public void ReleaseSteam(float timeSinceLastSteamRelease)
        {
            EngineLevels.SteamLevel -= timeSinceLastSteamRelease * _steamEngineRates.SteamReleaseRate;
            if (EngineLevels.SteamLevel < 0)
            {
                EngineLevels.SteamLevel = 0;
            }
        }
    }
}
