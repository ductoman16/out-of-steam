using System;

namespace Assets.Scripts.Train
{
    [Serializable]
    public class SteamEngineRates
    {
        public SteamEngineRates(int coalPerShovel, float coalBurnRate, float fireBurnDownRate, float waterToSteamRate, float steamReleaseRate)
        {
            CoalPerShovel = coalPerShovel;
            CoalBurnRate = coalBurnRate;
            FireBurnDownRate = fireBurnDownRate;
            WaterToSteamRate = waterToSteamRate;
            SteamReleaseRate = steamReleaseRate;
        }

        public int CoalPerShovel;
        public float CoalBurnRate;
        public float FireBurnDownRate;
        public float WaterToSteamRate;
        public float SteamReleaseRate;
    }
}