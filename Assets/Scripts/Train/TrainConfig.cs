﻿using System;

namespace Assets.Scripts.Train
{
    [Serializable]
    public class TrainConfig
    {
        public int PlowRefreshTime;
    }
}