﻿using System;
using System.Linq;

namespace Assets.Scripts.Locations
{
    //public interface ILocation
    //{
    //    string DisplayName { get; }
    //    //Action[] Actions { get; }
    //}

    public enum Location
    {
        Car,
        Tender,
        Cab,
        Boiler,
        Pilot
    }

    public static class LocationExtensions
    {
        public static bool IsLast(this Location location) { return location == Enum.GetValues(typeof(Location)).Cast<Location>().Last(); }
        public static bool IsFirst(this Location location) { return location == 0; }
    }
}
