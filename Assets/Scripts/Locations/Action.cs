﻿using System;
using System.Collections.Generic;
using Assets.Scripts.MonBehavior;
using Assets.Scripts.MonBehavior.Obstacles;
using Assets.Scripts.Obstacles;
using UnityEngine;

namespace Assets.Scripts.Locations
{
    public class Action
    {
        public Action(string displayName, Location location, KeyCode keyCode, Action<Train.Train, ObstacleManager> execute)
        {
            DisplayName = displayName;
            Location = location;
            KeyCode = keyCode;
            Execute = execute;
        }

        public string DisplayName { get; set; }
        public Location Location { get; set; }
        public KeyCode KeyCode { get; set; }
        public Action<Train.Train, ObstacleManager> Execute { get; set; }

        public static IEnumerable<Action> Actions = new List<Action>
        {
            new Action("Add Coal", Location.Cab, KeyCode.C, (train, ob) => train.Engine.BurnCoal()),
            new Action("Attach Pilot", Location.Pilot, KeyCode.Space, (train, ob) => train.AttachPlow()),
            new Action("> Shoot", Location.Car, KeyCode.Space, (train, ob) => ob.AvoidFirstObstacleOfType(ObstacleType.Robber)),
            new Action("Blow Horn", Location.Cab, KeyCode.Space, (train, ob) => ob.AvoidFirstObstacleOfType(ObstacleType.Cow))
        };
    }
}
