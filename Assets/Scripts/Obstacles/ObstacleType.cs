﻿namespace Assets.Scripts.Obstacles
{
    public enum ObstacleType
    {
        Debris,
        Robber,
        Cow
    }
}
