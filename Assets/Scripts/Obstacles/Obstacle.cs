﻿using System;
using Assets.Scripts.MonBehavior;

namespace Assets.Scripts.Obstacles
{
    public class Obstacle
    {
        public event EventHandler OnObstacleAvoid;
        public event EventHandler OnObstacleHit;

        public Obstacle(ObstacleType obstacleType, float distance)
        {
            ObstacleType = obstacleType;
            Distance = distance;
        }

        public ObstacleType ObstacleType { get; set; }

        /// <summary>
        ///     The absolute distance of the obstacle.
        /// </summary>
        public float Distance { get; set; }

        public void Avoid()
        {
            if (OnObstacleAvoid != null)
            {
                OnObstacleAvoid.Invoke(this, new EventArgs());
            }
        }

        public void Hit()
        {
            if (OnObstacleHit != null)
            {
                OnObstacleHit.Invoke(this, new EventArgs());
            }
        }
    }
}
