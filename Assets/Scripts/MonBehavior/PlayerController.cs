﻿using System.Linq;
using Assets.Scripts.Engineer;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior
{
    public class PlayerController : MonoBehaviour
    {
        public Sprite walkingSprite;
        public Sprite climbingSprite;
        public float MinMovementTimeDelta;
        public float PlayerSpeed = 1;

        private float _timeSinceLastMovementUpdate;

        private LocationController[] _locationControllers;
        private Engineer.Engineer _player;
        private GameStateManager _gameState;
        private SpriteRenderer _spriteRenderer;

        [Inject]
        public void Inject(Engineer.Engineer player, LocationController[] locationControllers, GameStateManager gameState)
        {
            _player = player;
            _locationControllers = locationControllers;
            _gameState = gameState;
        }

        // Use this for initialization
        void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _player.OnMovementStateChange += (s, e) => DoPlayerAnimation();
            DoPlayerAnimation();
        }

        private void DoPlayerAnimation()
        {
            switch (_player.MovementState)
            {
                case MovementState.Walking:
                    _spriteRenderer.sprite = walkingSprite;
                    _spriteRenderer.sortingOrder = -1;
                    break;
                case MovementState.Climbing:
                    _spriteRenderer.sprite = climbingSprite;
                    _spriteRenderer.sortingOrder = 2;
                    break;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_gameState.GameState == GameState.Active)
            {
                UpdatePlayerSpriteLocation();
            }
        }

        private void UpdatePlayerSpriteLocation()
        {
            var targetLocationController = _locationControllers.Single(l => l.Location == _player.TargetLocation);
            if (transform.position != targetLocationController.transform.position)
            {
                if (_timeSinceLastMovementUpdate > MinMovementTimeDelta)
                {
                    transform.position = Vector2.MoveTowards(transform.position,
                        targetLocationController.transform.position, _timeSinceLastMovementUpdate * PlayerSpeed);
                    _timeSinceLastMovementUpdate = 0;
                }
                else
                {
                    _timeSinceLastMovementUpdate += Time.deltaTime;
                }
            }
        }
    }
}
