﻿using System;
using Assets.Scripts.Obstacles;
using UnityEngine;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class RobberController : MonoBehaviour
    {
        public float DistanceToPopOut = .27F;

        public Obstacle Obstacle { get; set; }

        private bool _retreating = false;

        // Use this for initialization
        void Start()
        {
            Obstacle.OnObstacleAvoid += Retreat;
            Obstacle.OnObstacleHit += Retreat;
        }

        private void Retreat(object sender, EventArgs e)
        {
            _retreating = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (transform.localPosition.x < 0)
            {
                //Destroy ourselves if we're done
                Destroy(gameObject);
            }

            if (!_retreating)
            {
                //Move out
                transform.localPosition = Vector2.MoveTowards(transform.localPosition,
                    new Vector2(DistanceToPopOut, transform.localPosition.y),
                    Time.deltaTime);
            }
            else
            {
                //Move Back
                transform.localPosition = Vector2.MoveTowards(transform.localPosition,
                    new Vector2(0, transform.localPosition.y),
                    Time.deltaTime);
            }
        }
    }
}
