﻿using Assets.Scripts.Obstacles;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class BoulderController : MonoBehaviour
    {
        public float Speed = .05F;
        public GameObject SmallBoulderPrefab;
        public int SmallBouldersToCreate = 3;
        public float SmallBoulderVelocity = 200;

        public Obstacle Obstacle { get; set; }

        private Train.Train _train;

        [Inject]
        public void Inject(Train.Train train)
        {
            _train = train;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            transform.position = Vector2.MoveTowards(transform.position, Vector2.left, Time.deltaTime * Speed * _train.Speed);
        }

        void OnTriggerEnter2D(Collider2D collision)
        {
            for (int i = 0; i < SmallBouldersToCreate; i++)
            {
                var newPosition = transform.position;
                newPosition.x += Random.value;
                var smallBoulder = Instantiate(SmallBoulderPrefab, newPosition, Quaternion.identity);
                smallBoulder.GetComponent<Rigidbody2D>().AddForce(Vector2.left * SmallBoulderVelocity);
            }
            Destroy(gameObject);
        }
    }
}
