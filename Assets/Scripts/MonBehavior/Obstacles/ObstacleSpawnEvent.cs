﻿using System;
using Assets.Scripts.Obstacles;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class ObstacleEventArgs : EventArgs
    {
        public ObstacleEventArgs(Obstacle obstacle)
        {
            Obstacle = obstacle;
        }

        public Obstacle Obstacle { get; set; }
    }
}
