﻿using UnityEngine;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class ObstacleWarningController : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Hit()
        {
            //TODO: Animations!
            Destroy(gameObject);
        }

        public void Avoid()
        {
            Destroy(gameObject);
        }
    }
}
