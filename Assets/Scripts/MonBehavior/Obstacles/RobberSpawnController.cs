﻿using Assets.Scripts.Obstacles;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class RobberSpawnController : MonoBehaviour
    {
        public GameObject RobberPrefab;

        private ObstacleManager _obstacleManager;

        [Inject]
        public void Inject(ObstacleManager obstacleManager)
        {
            _obstacleManager = obstacleManager;
        }

        // Use this for initialization
        void Start()
        {
            _obstacleManager.OnObstacleSpawn += (sender, args) =>
            {
                if (args.Obstacle.ObstacleType == ObstacleType.Robber)
                {
                    //Make a new robber to pop up
                    var robber = Instantiate(RobberPrefab, transform);
                    robber.GetComponent<RobberController>().Obstacle = args.Obstacle;
                }
            };
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
