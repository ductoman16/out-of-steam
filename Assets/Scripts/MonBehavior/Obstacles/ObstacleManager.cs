﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Obstacles;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Random = UnityEngine.Random;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class ObstacleManager : MonoBehaviour
    {
        public int ObstacleFrequencyPercent;
        public GameObject WarningSpawnParent;
        public float WarningOffsetY = .5F;
        public ObstaclePrefabMapping[] ObstaclePrefabMapping;
        public int ObstacleDistance = 150;
        public int MaxObstaclesOfType = 3;
        public int CoalLossPerHit = 2;

        private readonly IDictionary<Obstacle, ObstacleWarningController> _obstacles = new Dictionary<Obstacle, ObstacleWarningController>();

        public event EventHandler<ObstacleEventArgs> OnObstacleSpawn;


        private Train.Train _train;
        private GameStateManager _gameState;

        [Inject]
        public void Inject(Train.Train train, GameStateManager gameState)
        {
            _train = train;
            _gameState = gameState;
        }

        // Update is called once per frame
        void Update()
        {
            if (_gameState.GameState == GameState.Active)
            {
                GenerateObstacles();
                UpdateObstacles();
                UpdateObstaclesDisplays();
            }
        }

        private void UpdateObstacles()
        {
            foreach (var obstacle in _obstacles.ToList())
            {
                if (obstacle.Key.Distance < _train.DistanceTravelled)
                {
                    HitObstacle(obstacle.Key, obstacle.Value);
                }
            }
        }

        private void HitObstacle(Obstacle obstacle, ObstacleWarningController obstacleWarningController)
        {
            var obstacleArgs = new ObstacleEventArgs(obstacle);
            switch (obstacle.ObstacleType)
            {
                case ObstacleType.Debris:
                    if (!_train.HasPlow)
                    {
                        _train.Hit(obstacle.ObstacleType, CoalLossPerHit);
                        obstacleWarningController.Hit();
                        obstacle.Hit();
                    }
                    else
                    {
                        obstacleWarningController.Avoid();
                        obstacle.Avoid();
                    }
                    break;
                case ObstacleType.Robber:
                case ObstacleType.Cow:
                    _train.Hit(obstacle.ObstacleType, CoalLossPerHit);
                    obstacleWarningController.Hit();
                    obstacle.Hit();
                    break;
            }
            _obstacles.Remove(obstacle);
        }

        private void UpdateObstaclesDisplays()
        {
            foreach (var obstacle in _obstacles)
            {
                var text = obstacle.Value.GetComponentInChildren<Text>();
                text.text = (obstacle.Key.Distance - _train.DistanceTravelled).ToString("###");
            }
        }

        private void GenerateObstacles()
        {
            var random = Random.Range(0, 1000);
            if (random < ObstacleFrequencyPercent)
            {
                SpawnRandomObstacle();
            }
        }

        private void SpawnRandomObstacle()
        {
            var obstacleTypeToSpawn = (ObstacleType)Random.Range(0, Enum.GetValues(typeof(ObstacleType)).Length);
            if (_obstacles.Count(o => o.Key.ObstacleType == obstacleTypeToSpawn) >= MaxObstaclesOfType)
            {
                return;
            }
            var prefab = ObstaclePrefabMapping.Single(o => o.ObstacleType == obstacleTypeToSpawn).WarningPrefab;
            var instance = Instantiate(prefab, WarningSpawnParent.transform);
            var position = instance.transform.position;
            var obstaclesOfTypeCount = _obstacles.Count(o => o.Key.ObstacleType == obstacleTypeToSpawn);
            instance.transform.position = new Vector3(position.x, position.y - WarningOffsetY * obstaclesOfTypeCount, position.z);
            var spawnedObstacle = new Obstacle(obstacleTypeToSpawn, _train.DistanceTravelled + ObstacleDistance);
            _obstacles.Add(spawnedObstacle, instance.GetComponent<ObstacleWarningController>());

            if (OnObstacleSpawn != null)
            {
                OnObstacleSpawn.Invoke(this, new ObstacleEventArgs(spawnedObstacle));
            }
        }

        public void AvoidFirstObstacleOfType(ObstacleType obstacleType)
        {
            var obstaclesOfType = _obstacles.Where(o => o.Key.ObstacleType == obstacleType).ToList();

            if (obstaclesOfType.Any())
            {
                var firstObstacleOfType = obstaclesOfType.First();
                _obstacles.Remove(firstObstacleOfType.Key);
                firstObstacleOfType.Value.Avoid();
                firstObstacleOfType.Key.Avoid();
            }

        }
    }
}
