﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Obstacles;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class CowSpawnController : MonoBehaviour
    {
        public GameObject CowPrefab;

        private ObstacleManager _obstacleManager;
        private DiContainer _diContainer;

        [Inject]
        public void Inject(DiContainer container, ObstacleManager obstacleManager)
        {
            _obstacleManager = obstacleManager;
            _diContainer = container;
        }

        // Use this for initialization
        void Start()
        {
            _obstacleManager.OnObstacleSpawn += (sender, args) =>
            {
                if (args.Obstacle.ObstacleType == ObstacleType.Cow)
                {
                    args.Obstacle.OnObstacleHit += (s, eventArgs) =>
                    {
                        var cow = _diContainer.InstantiatePrefab(CowPrefab, transform);
                        //boulder.GetComponent<CowController>().Obstacle = args.Obstacle;
                    };
                }
            };
        }

    }
}
