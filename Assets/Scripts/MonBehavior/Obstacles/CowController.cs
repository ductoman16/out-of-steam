﻿using UnityEngine;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class CowController: MonoBehaviour
    {
        public float Speed = 200;
        public float RotateSpeed = 100;

        void Start()
        {
            var rigidBody2D = GetComponent<Rigidbody2D>();
            rigidBody2D.AddForce(Vector2.left * Speed);
            rigidBody2D.AddForce(Vector2.up * Speed /2);
            rigidBody2D.AddTorque(RotateSpeed);
        }
    }
}
