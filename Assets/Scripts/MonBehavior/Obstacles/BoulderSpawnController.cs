﻿using Assets.Scripts.Obstacles;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior.Obstacles
{
    public class BoulderSpawnController : MonoBehaviour
    {
        public GameObject BoulderPrefab;

        private ObstacleManager _obstacleManager;
        private DiContainer _diContainer;

        [Inject]
        public void Inject(DiContainer container, ObstacleManager obstacleManager)
        {
            _obstacleManager = obstacleManager;
            _diContainer = container;
        }

        // Use this for initialization
        void Start()
        {
            _obstacleManager.OnObstacleSpawn += (sender, args) =>
            {
                if (args.Obstacle.ObstacleType == ObstacleType.Debris)
                {
                    args.Obstacle.OnObstacleHit += (s, eventArgs) =>
                    {
                        var boulder = _diContainer.InstantiatePrefab(BoulderPrefab, transform);
                        //var boulder = Instantiate(BoulderPrefab, transform);
                        boulder.GetComponent<BoulderController>().Obstacle = args.Obstacle;
                    };
                }
            };
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
