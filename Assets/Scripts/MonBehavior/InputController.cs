﻿using System.Collections.Generic;
using Assets.Scripts.Locations;
using Assets.Scripts.MonBehavior.Obstacles;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior
{
    public class InputController : MonoBehaviour
    {
        private Engineer.Engineer _player;
        private Train.Train _train;
        private IEnumerable<Action> _actions;
        private ObstacleManager _obstacleManager;
        private GameStateManager _gameState;

        [Inject]
        public void Inject(Engineer.Engineer player, Train.Train train, IEnumerable<Action> actions, ObstacleManager obstacleManager, GameStateManager gameState)
        {
            _player = player;
            _train = train;
            _actions = actions;
            _obstacleManager = obstacleManager;
            _gameState = gameState;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_gameState.GameState == GameState.Active)
            {
                if (Input.GetKeyUp(KeyCode.LeftArrow))
                {
                    _player.StartMovePrevious();
                }
                if (Input.GetKeyUp(KeyCode.RightArrow))
                {
                    _player.StartMoveNext();
                }
                foreach (var action in _actions)
                {
                    if (Input.GetKeyUp(action.KeyCode) && _player.CurrentLocation == action.Location)
                    {
                        action.Execute(_train, _obstacleManager);
                    }
                }
            }
        }
    }
}
