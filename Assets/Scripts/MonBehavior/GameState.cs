﻿namespace Assets.Scripts.MonBehavior
{
    public enum GameState
    {
        Inactive,
        Active,
        Won,
        Lost
    }

    public class GameStateManager
    {
        public GameStateManager()
        {
            GameState = GameState.Active;
        }
        public GameState GameState { get; set; }
    }
}