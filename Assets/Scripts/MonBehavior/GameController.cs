﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MonBehavior
{
    public class GameController : MonoBehaviour
    {
        public int DistanceToWin;

        [Header("Messages")]
        public Text WinMessage;
        public Text LoseMessage;
        public Text TitleText;

        public GameObject TitleCard;

        [Header("Audio")]
        public AudioClip TrainSlow;
        public AudioClip TrainMedium;
        public AudioClip TrainFast;
        public AudioClip TrainVeryFast;

        private GameStateManager _gameStateManager;

        private Train.Train _train;
        private AudioSource _audioSource;

        [Inject]
        public void Inject(Train.Train train, GameStateManager gameStateManager)
        {
            _train = train;
            _gameStateManager = gameStateManager;
        }

        private void Awake()
        {
            //TODO: Stay inactive until player is ready
            _gameStateManager.GameState = GameState.Inactive;
            TitleCard.gameObject.SetActive(true);
        }

        void Start()
        {
            _audioSource = GetComponent<AudioSource>();
            if (!_audioSource.isPlaying)
            {
                _audioSource.Play();
            }
        }

        // Update is called once per frame
        void Update()
        {
            DoSounds();
            CheckForStart();
            CheckForWin();
            CheckForLoss();
        }

        private void DoSounds()
        {
            //TODO: This is sooooo hard coded

            if (_train.Speed <= 0)
            {
                _audioSource.Stop();
            }
            else
            {
                if (!_audioSource.isPlaying)
                {
                    _audioSource.Play();
                }
            }

            if (_train.Speed > 15)
            {
                _audioSource.clip = TrainVeryFast;
            }
            else if (_train.Speed <= 15 && _train.Speed > 10)
            {
                _audioSource.clip = TrainFast;
            }
            else if (_train.Speed <= 10 && _train.Speed > 5)
            {
                _audioSource.clip = TrainMedium;
            }
            else
            {
                _audioSource.clip = TrainSlow;
            }
        }

        private void CheckForStart()
        {
            if (_gameStateManager.GameState == GameState.Inactive)
            {
                if (Input.GetKeyUp(KeyCode.Space))
                {
                    StartGame();
                }
            }
        }

        private void StartGame()
        {
            _gameStateManager.GameState = GameState.Active;
            TitleCard.SetActive(false);
            TitleText.gameObject.SetActive(false);
        }

        private void CheckForWin()
        {
            if (_train.DistanceTravelled >= DistanceToWin)
            {
                //You win!
                TitleCard.gameObject.SetActive(true);
                WinMessage.gameObject.SetActive(true);
                _gameStateManager.GameState = GameState.Won;
            }
        }

        private void CheckForLoss()
        {
            if (_train.Speed <= 0)
            {
                //You lose!
                TitleCard.gameObject.SetActive(true);
                LoseMessage.gameObject.SetActive(true);
                _gameStateManager.GameState = GameState.Lost;
            }
        }

    }
}
