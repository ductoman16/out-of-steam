﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior
{
    /// <summary>
    ///     Updates train behavior every frame.
    /// </summary>
    public class TrainController : MonoBehaviour
    {
        public GameObject Plow;

        private Train.Train _train;
        private GameStateManager _gameState;

        [Inject]
        public void Inject(Train.Train train, GameStateManager gameState)
        {
            _train = train;
            _gameState = gameState;
        }

        public float Speed { get { return _train.Speed; } }

        // Update is called once per frame
        void Update()
        {
            if (_gameState.GameState == GameState.Active)
            {
                UpdateDistanceTravelled();
                _train.Engine.BurnWater(Time.deltaTime);
                _train.Engine.FireBurnDown(Time.deltaTime);
                _train.Engine.ReleaseSteam(Time.deltaTime);
                _train.DecayPlow(Time.deltaTime);
                Plow.SetActive(_train.HasPlow);
            }
        }

        private void UpdateDistanceTravelled()
        {
            _train.DistanceTravelled += Speed * Time.deltaTime;
        }
    }
}
