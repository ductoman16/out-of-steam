﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonBehavior.UI
{
    public class CoalCountController : MonoBehaviour
    {
        public Text Text;
        private Train.Train _train;

        [Zenject.Inject]
        public void Inject(Train.Train train)
        {
            _train = train;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Text.text = _train.Engine.EngineLevels.CoalLevel.ToString();
        }
    }
}
