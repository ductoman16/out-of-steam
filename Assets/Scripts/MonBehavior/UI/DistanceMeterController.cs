﻿using UnityEngine;

namespace Assets.Scripts.MonBehavior.UI
{
    public class DistanceMeterController : MonoBehaviour
    {
        public float StartX;
        public float EndX;

        private Train.Train _train;
        private GameController _gameController;

        [Zenject.Inject]
        public void Inject(Train.Train train, GameController gameController)
        {
            _train = train;
            _gameController = gameController;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            var xRange = EndX - StartX;

            var distancePercent = _train.DistanceTravelled / _gameController.DistanceToWin;

            var newXDistance = xRange * distancePercent;

            Vector3 localPos = transform.localPosition;
            localPos.x = StartX + newXDistance;
            transform.localPosition = localPos;
        }
    }
}
