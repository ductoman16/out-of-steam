﻿using UnityEngine;

namespace Assets.Scripts.MonBehavior.UI
{
    public class WaterDisplayController : MonoBehaviour
    {
        public float MinScaleY;
        public float MaxScaleY;

        private Train.Train _train;
        private float _initialWaterLevel;

        [Zenject.Inject]
        public void Inject(Train.Train train)
        {
            _train = train;
            _initialWaterLevel = train.Engine.EngineLevels.WaterLevel; //Save the water level before it changes. 
            //TODO: don't have time to figure out if the water level is always untouched right when it gets injected. Seems like a good assumption though.
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            var scaleRange = MaxScaleY - MinScaleY;
            var waterPercent =  _train.Engine.EngineLevels.WaterLevel / _initialWaterLevel;

            var localScale = transform.localScale;
            localScale.y = MinScaleY + scaleRange * waterPercent;
            transform.localScale = localScale;
        }
    }
}
