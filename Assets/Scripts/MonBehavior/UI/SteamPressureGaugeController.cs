﻿using UnityEngine;

namespace Assets.Scripts.MonBehavior.UI
{
    public class SteamPressureGaugeController : MonoBehaviour
    {
        public float MinAngle = -180;
        public float MaxAngle = 90;

        private Train.Train _train;

        [Zenject.Inject]
        public void Inject(Train.Train train)
        {
            _train = train;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

            var angleRange = MaxAngle - MinAngle;

            var steamPercent = _train.Engine.EngineLevels.SteamLevel / _train.Engine.EngineLevels.MaxSteam;

            var newAngle = angleRange * steamPercent;

            transform.rotation = Quaternion.AngleAxis(MinAngle + newAngle, Vector3.back);
        }
    }
}
