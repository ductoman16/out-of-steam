﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MonBehavior.UI
{
    public class LocationDisplayController : MonoBehaviour
    {

        private Engineer.Engineer _player;
        private LocationController[] _locationControllers;
        public Text PreviousLocation;
        public Text LeftIndicator;
        public Text CurrentLocation;
        public Text RightIndicator;
        public Text NextLocation;

        [Inject]
        public void Inject(Engineer.Engineer player, LocationController[] locationControllers)
        {
            _player = player;
            _locationControllers = locationControllers;
        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            var currentLocationController = _locationControllers.Single(l => l.Location == _player.CurrentLocation);
            var currentLocationControllerIndex = Array.IndexOf(_locationControllers, currentLocationController);

            PreviousLocation.text = "";
            NextLocation.text = "";
            LeftIndicator.gameObject.SetActive(false);
            RightIndicator.gameObject.SetActive(false);

            if (_player.CanMovePrevious)
            {
                var previousLocationController =
                    _locationControllers[currentLocationControllerIndex - 1];
                PreviousLocation.text = previousLocationController.gameObject.name;
                LeftIndicator.gameObject.SetActive(true);
            }

            var currentLocationName = currentLocationController.gameObject.name; //TODO: there should be a displayname on each locationController
            CurrentLocation.text = currentLocationName;

            if (_player.CanMoveNext)
            {
                var nextLocationController = _locationControllers[currentLocationControllerIndex + 1];
                NextLocation.text = nextLocationController.gameObject.name;
                RightIndicator.gameObject.SetActive(true);
            }
        }
    }
}
