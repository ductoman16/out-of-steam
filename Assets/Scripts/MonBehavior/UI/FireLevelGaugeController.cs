﻿using UnityEngine;

namespace Assets.Scripts.MonBehavior.UI
{
    public class FireLevelGaugeController : MonoBehaviour
    {
        public float MinAngle = -180;
        public float MaxAngle = 90;

        public float MaxFireLevel = 10;

        private Train.Train _train;

        [Zenject.Inject]
        public void Inject(Train.Train train)
        {
            _train = train;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            var angleRange = MaxAngle - MinAngle;

            var steamPercent = _train.Engine.EngineLevels.FireLevel / MaxFireLevel;

            var newAngle = Mathf.Clamp(MinAngle + angleRange * steamPercent, MinAngle, MaxAngle);
            transform.rotation = Quaternion.AngleAxis(newAngle, Vector3.back);
        }
    }
}
