﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior.UI
{
    public class BackgroundScroller : MonoBehaviour
    {
        public float ScrollSpeedMultiplier;
        public float TileSizeZ;

        private Train.Train _train;
        private Vector2 _startPosition;

        [Inject]
        public void Inject(Train.Train train)
        {
            _train = train;
        }

        void Start()
        {
            _startPosition = transform.position;
        }

        void Update()
        {
            float newPosition = Mathf.Repeat(_train.DistanceTravelled * ScrollSpeedMultiplier, TileSizeZ);
            transform.position = _startPosition + Vector2.left * newPosition;
        }
    }
}