﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Locations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MonBehavior.UI
{
    public class ActionDisplayController : MonoBehaviour
    {
        private Text _textComponent;

        private Engineer.Engineer _player;
        private IEnumerable<Action> _actions;

        [Inject]
        public void Inject(Engineer.Engineer player, IEnumerable<Action> actions)
        {
            _player = player;
            _actions = actions;
        }
        // Use this for initialization
        void Start()
        {
            _textComponent = GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            _textComponent.text = "";
            foreach (var action in _actions.Where(a => a.Location == _player.CurrentLocation))
            {
                _textComponent.text += action.DisplayName + "(" + action.KeyCode + ")   ";
            }
        }
    }
}
