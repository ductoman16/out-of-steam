﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior.UI
{
    public class WheelRotator : MonoBehaviour
    {
        public float WheelRotateMultiplier = 1;

        private Train.Train _train;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(Train.Train train, GameStateManager gameStateManager)
        {
            _train = train;
            _gameStateManager = gameStateManager;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_gameStateManager.GameState == GameState.Active)
            {
                transform.Rotate(Vector3.back, _train.Speed * WheelRotateMultiplier);
            }
        }
    }
}
