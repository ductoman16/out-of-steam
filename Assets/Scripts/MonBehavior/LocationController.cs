﻿using Assets.Scripts.Locations;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior
{
    public class LocationController : MonoBehaviour
    {
        public Location Location;

        private Engineer.Engineer _player;

        [Inject]
        public void Inject(Engineer.Engineer engineer)
        {
            _player = engineer;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Player") //TODO: Magic string
            {
                _player.CurrentLocation = Location;
            }
        }
    }
}
