﻿using Assets.Scripts.Engineer;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MonBehavior
{
    public class AnimationAreaController : MonoBehaviour
    {
        public MovementState MovementState;

        private Engineer.Engineer _player;

        [Inject]
        public void Inject(Engineer.Engineer player)
        {
            _player = player;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                _player.SetMovementState(MovementState);
            }
        }
    }
}
