﻿using System;
using Assets.Scripts.Obstacles;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class ObstaclePrefabMapping
    {
        public ObstacleType ObstacleType;
        public GameObject WarningPrefab;
    }
}
